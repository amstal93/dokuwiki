DokuWiki Docker Container
=========================

_*DokuWiki is a simple to use and highly versatile open source wiki software that doesn't require a database. It is loved by users for its clean and readable syntax. The ease of maintenance, backup and integration makes it an administrator's favorite. Built in access controls and authentication connectors make DokuWiki especially useful in the enterprise context and the large number of plugins contributed by its vibrant community allow for a broad range of use cases beyond a traditional wiki.*_

This container aims to be easy to understand, easy to backup/restore, and easy to operate. This container also supports amd64 and armhf architectures!

Any suggestions on improving this containers ease of use are welcome.

Running DokuWiki
-----------------

    docker run -d -p 80:80 --name dokuwiki \
        -v /data/docker/dokuwiki/data:/dokuwiki/data \
        -v /data/docker/dokuwiki/conf:/dokuwiki/conf \
        -v /data/docker/dokuwiki/lib/plugins:/dokuwiki/lib/plugins \
        -v /data/docker/dokuwiki/lib/tpl:/dokuwiki/lib/tpl \
        -v /data/docker/dokuwiki/logs:/var/log \
        registry.gitlab.com/sparky8251/dokuwiki

If this is the first time creating a DokuWiki using the above volumes,
you can access the installer by going to http://127.0.0.1/install.php

If this is pointing to an existing wiki install you can access the wiki by going to
http://127.0.0.1/

### Run a specific version ###

When running the container you can specify version to download from docker registry by using couple provided tags like *stable* which contains current stable release (this is generally the same as *latest*). You can also use specific version like *2017-02-19e*.

### Running armhf containers ###

When running the container, specify a tag like *stabe-armhf*.

Upgrading/Downgrading DokuWiki
------------------------------

### Upgrading ###

Just stop the old container and run the new one with the same `-v` mappings. Since the data is on your docker host, you do not need to do anything special to preserve it between containers.

### Downgrading ###

**Note:** The functionality bellow is currently only supported in *latest*, *stable* and *2017-02-19e* tags. This is to preserve original functionality in old images. Future releases will all support the new update system.

If you mount a volume that has previously been used with a newer version of DokuWiki than that installed in the current container, the newer files will _not_ be overwritten by those bundled with the current (older) version of DokuWiki. If you want to force a downgrade (at your own risk!), run the container with the `downgrade` command:

    docker run -d -p 80:80 --name dokuwiki \
        -v /data/docker/dokuwiki/data:/dokuwiki/data \
        -v /data/docker/dokuwiki/conf:/dokuwiki/conf \
        -v /data/docker/dokuwiki/lib/plugins:/dokuwiki/lib/plugins \
        -v /data/docker/dokuwiki/lib/tpl:/dokuwiki/lib/tpl \
        -v /data/docker/dokuwiki/logs:/var/log \
        registry.gitlab.com/sparky8251/dokuwiki downgrade

Additionally, if you make any changes to the files bundled with DokuWiki that are located in your volumes, these can be restored to the original version using the `overwrite` command:

    docker run -d -p 80:80 --name dokuwiki \
        -v /data/docker/dokuwiki/data:/dokuwiki/data \
        -v /data/docker/dokuwiki/conf:/dokuwiki/conf \
        -v /data/docker/dokuwiki/lib/plugins:/dokuwiki/lib/plugins \
        -v /data/docker/dokuwiki/lib/tpl:/dokuwiki/lib/tpl \
        -v /data/docker/dokuwiki/logs:/var/log \
        registry.gitlab.com/sparky8251/dokuwiki overwrite

Lighttpd tweaks for DokuWiki
----------------------------

The Lighttpd configuration includes support for Dokuwiki's Nice URLs.
You can enable Nice URLs in your wiki settings at:

Admin -> Configuration Manager -> Advanced

userewrite -> set to "".htaccess"

For better performance enable xsendfile in your wiki settings at:

Admin -> Configuration Manager -> Advanced

xsendfile -> Set to "Proprietary lighttpd header (before release 1.5)"
